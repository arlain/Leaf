package com.sankuai.inf.leaf.segment;

import com.sankuai.inf.leaf.IDGen;
import com.sankuai.inf.leaf.common.Result;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"}) //加载配置文件
public class SpringIDGenServiceTest {

    @Resource(name = "leaf")
    IDGen idGen;

    @Resource(name = "leafSnow")
    IDGen idGenSnow;

    private static final int N = 20;
    private static final int M = 1000;

    private ExecutorService executors = Executors.newFixedThreadPool(N);
    private CountDownLatch latch = new CountDownLatch(N);

    @Test
    public void testGetIdDb() {
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            Result r = idGen.get("leaf-segment-test");
            System.out.println(r + "     --------      " + Thread.currentThread().getName());
        }
    }


    @Test
    public void testGetId() throws InterruptedException {

        final Set<Long> ids = new HashSet<>(N * M);
        for (int j = 0; j < N; j++) {
            executors.execute(new Runnable() {
                @Override
                public void run() {
                    latch.countDown();
                    try {
                        latch.await();
                        for (int i = 0; i < M; i++) {
                            Result r = idGenSnow.get("leaf-segment-test");

                            // Set add() 方法，未包含返回 true ，包含-替换(Map)-返回 false
                            /* HashMap add 方法
                            private static final Object PRESENT = new Object();

                            public boolean add(E e) {
                                return map.put(e, PRESENT)==null;
                            }
                             */
                            if (!ids.add(r.getId())) {
                                System.out.println(r + "     --------      " + Thread.currentThread().getName());
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        Thread.sleep(20000);
    }
}
